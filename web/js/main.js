
// Инициализация слайдера цены
var priceSpan = $('.valueSpan2');
var priceValue = $('#customRange11');

priceValue.on('input change', () => {
    priceSpan.html(priceValue.val());
});


$(document).ready(function () {

    checkProducts();

    $('.filterBox , .priceBtn ').click(function () {
        applyFilters();
    });

    $('#removeFilters').click(function () {
        setMaxPriceVal();
        $('.filterBox').each(function () {
            $(this).prop('checked', false);
            getProducts(true);
            localStorage.clear();
        });
    });


});

function setMaxPriceVal() {
    priceValue.val(priceValue.attr('max'));
    priceSpan.html(priceValue.val())
}

function checkProducts() {

    if (!localStorage.restoreProducts) {
        setMaxPriceVal();
        getProducts(true);

    } else {
        try {
            priceValue.val(localStorage.getItem('price'));
            priceSpan.html(priceValue.val());

            var savedCheckboxes = JSON.parse(localStorage.checkboxes);
            savedCheckboxes.forEach(function(key) {
                $('.filterBox[data-sql='+key+']').prop('checked', true);
            });
            getProducts(false, JSON.parse(localStorage.products));
        }
        catch (e) {
        }

    }
}

function getProducts(loadAll, data) {
    if (loadAll) data = {};
    $.ajax({
        url: "/ajax/cards",
        data: data,
        type: 'POST',
        success: function (response) {
            $('#inline-product-list').html(response);
        }
    })
}

function applyFilters() {
    var filterList = {
        manufacture_id: '',
        mem_amount: '',
        bandwidth: '',
        price: 0
    };

    var checkboxes = [];

    $('.filterBox').each(function () {
        if ($(this).is(":checked")) {
            filterList[this.name] += this.value + ',';
            checkboxes.push($(this).attr('data-sql'));
        }

    });
    filterList.price = priceSpan.text();
    localStorage.setItem("restoreProducts", '1');
    localStorage.setItem("price", priceValue.val());
    localStorage.setItem("products", JSON.stringify(filterList));
    localStorage.setItem("checkboxes", JSON.stringify(checkboxes));
    getProducts(false, filterList);
}

