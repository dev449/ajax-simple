-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Май 19 2020 г., 14:40
-- Версия сервера: 10.0.33-MariaDB
-- Версия PHP: 7.1.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `php-test`
--

-- --------------------------------------------------------

--
-- Структура таблицы `cards`
--

CREATE TABLE `cards` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `manufacture_id` smallint(6) NOT NULL,
  `price` int(11) NOT NULL,
  `mem_amount` smallint(6) NOT NULL,
  `bandwidth` smallint(6) NOT NULL,
  `thumb` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `cards`
--

INSERT INTO `cards` (`id`, `name`, `manufacture_id`, `price`, `mem_amount`, `bandwidth`, `thumb`) VALUES
(5, 'Asus PCI-Ex GeForce GT 710', 1, 1069, 1, 32, 'gt710'),
(6, 'Asus PCI-Ex GeForce GTX 1050 Ti ROG Strix', 1, 4799, 4, 128, 'gtx1050'),
(7, 'Asus PCI-Ex GeForce RTX 2070 Super ROG Strix', 1, 19814, 8, 256, 'RTX2070'),
(8, 'Gigabyte PCI-Ex GeForce GTX 1650 Super OC', 3, 5059, 4, 128, 'gtx 1650'),
(9, 'Gigabyte PCI-Ex GeForce GTX 1660 Super OC', 3, 7699, 6, 192, 'gtx1660'),
(10, 'Gigabyte PCI-Ex GeForce RTX 2070 Super Gaming OC 3X White', 3, 16405, 8, 256, 'rtx2070'),
(15, 'MSI PCI-Ex GeForce GTX 1050 Ti GAMING X', 4, 4779, 4, 128, 'gtx1050'),
(16, 'MSI PCI-Ex GeForce RTX 2070 Super Gaming X', 4, 17759, 8, 256, 'rtx2070'),
(17, 'MSI PCI-Ex Radeon RX 570 ARMOR', 4, 5255, 8, 256, 'rx570'),
(18, 'Sapphire PCI-Ex Radeon RX 5500 XT NITRO', 2, 6799, 8, 128, 'rx5500'),
(19, 'Sapphire PCI-Ex Radeon RX 5600 XT Pulse', 2, 9799, 6, 192, 'rx5600'),
(20, 'Sapphire PCI-Ex Radeon RX 5700 XT NITRO+ Special Edition', 2, 14328, 8, 256, 'rx5700');

-- --------------------------------------------------------

--
-- Структура таблицы `manufacturer`
--

CREATE TABLE `manufacturer` (
  `id` smallint(6) NOT NULL,
  `name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `manufacturer`
--

INSERT INTO `manufacturer` (`id`, `name`) VALUES
(1, 'asus'),
(2, 'sapphire'),
(3, 'gigabyte'),
(4, 'msi');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `cards`
--
ALTER TABLE `cards`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `manufacture_id` (`manufacture_id`);

--
-- Индексы таблицы `manufacturer`
--
ALTER TABLE `manufacturer`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `cards`
--
ALTER TABLE `cards`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT для таблицы `manufacturer`
--
ALTER TABLE `manufacturer`
  MODIFY `id` smallint(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `cards`
--
ALTER TABLE `cards`
  ADD CONSTRAINT `cards_ibfk_1` FOREIGN KEY (`manufacture_id`) REFERENCES `manufacturer` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
