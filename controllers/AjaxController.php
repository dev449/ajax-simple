<?php


namespace app\controllers;


use yii\base\Controller;
use app\models\Cards;

class AjaxController extends Controller
{
    public $html = '';
    public $templatePath = '../views/template/product.php';
    public $sqlParams = [
        'manufacture_id',
        'mem_amount',
        'bandwidth',
        'price'
    ];


    public function actionCards()
    {

        $cardsRequest = Cards::find()->orderBy('price');

        foreach ($this->sqlParams as $sqlParam) {
            if (!empty($_POST[$sqlParam])) {
                if ($sqlParam == 'price') $cardsRequest->andWhere(['<=', $sqlParam, intval($_POST[$sqlParam])]);
               else $cardsRequest->andWhere([$sqlParam => explode(',',$_POST[$sqlParam])]);
            }

        }
            $cards = $cardsRequest->all();
            foreach ($cards as $card) {
                $this->html .= sprintf(include($this->templatePath),
                    $card->manufacturer->name, $card->thumb, $card->name,
                    $card->price,$card->mem_amount,$card->bandwidth);
            }
             return $this->html;
        }

}

