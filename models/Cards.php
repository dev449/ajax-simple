<?php


namespace app\models;


use yii\db\ActiveRecord;

class Cards extends ActiveRecord
{
    public $man_name;

    public function getManufacturer() {

        return $this->hasOne(Manufacturer::className(), ['id' => 'manufacture_id']);

    }

    public static function CardsProps()
    {
        return Cards::find()
            ->select([
                'cards.manufacture_id',
                'cards.mem_amount',
                'cards.bandwidth',
                'man_name' => 'manufacturer.name'])
            ->innerJoin('manufacturer', 'cards.manufacture_id = manufacturer.id')
            ->all();
    }
}