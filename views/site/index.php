<?php

use app\models\Cards;
use app\models\Manufacturer;


$cards = Cards::CardsProps();


$manufactureList = Manufacturer::find()->orderBy('name')->all();
$memAmountList = array_unique(array_column($cards, 'mem_amount'), SORT_REGULAR);
$bandwidthList = array_unique(array_column($cards, 'bandwidth'), SORT_REGULAR);

sort($memAmountList);
sort($bandwidthList);
?>

<div id="wrap-content">
    <div id="main-content">
        <div id="left-column">

            <div class="left-block">
                <button id="removeFilters" onclick="this.blur();" class="btn btn-primary">Сбросить Фильтры</button>
            </div>

            <div class="left-block">
                <div class="wrap-title-black">
                    <h2 class="nice-title long">Цена</h2>
                </div>
                <div class="block-content filter-block">


                    <span class="font-weight-bold p-5 float-left">₴<?= Cards::find()->min('price') ?></span>
                    <span class="font-weight-bold p-5 float-right">₴<?= Cards::find()->max('price') ?></span>
                    <div class="d-flex justify-content-center my-4">
                        <div class="w-75">
                            <input type="range" class="custom-range" id="customRange11"
                                   min="<?= Cards::find()->min('price') ?>"
                                   max="<?= Cards::find()->max('price') ?>">
                        </div>
                        <span class="font-weight-bold d-block text-center  valueSpan2"></span>
                    </div>
                    <button id="applyPrice" onclick="this.blur();" class="btn priceBtn btn-success">Применить</button>

                </div>
            </div>

            <div class="left-block">
                <div class="wrap-title-black">
                    <h2 class="nice-title long">Производитель</h2>
                </div>
                <div class="block-content filter-block">
                    <form id="brand-form" action="" method="post">
                        <?php

                        foreach ($manufactureList as $manufacturer) : ?>
                            <div class="checkline">
                                <input data-sql="man_<?= $manufacturer->id ?>" class="filterBox"
                                       type="checkbox" name="manufacture_id"
                                       value="<?= $manufacturer->id ?>"/><span><?= ucfirst($manufacturer->name) ?></span>
                            </div>
                        <?php endforeach;
                        ?>

                    </form>
                </div>
            </div>

            <div class="left-block">
                <div class="wrap-title-black">
                    <h2 class="nice-title long">Объем Памяти</h2>
                </div>
                <div class="block-content filter-block">
                    <form id="brand-form" action="" method="post">


                        <?php

                        foreach ($memAmountList as $memAmount) : ?>
                            <div class="checkline">
                                <input data-sql="mem_<?= $memAmount ?>" class="filterBox"
                                       type="checkbox" name="mem_amount"
                                       value="<?= $memAmount ?>"/><span><?= $memAmount ?></span>
                            </div>
                        <?php endforeach; ?>

                    </form>
                </div>
            </div>

            <div class="left-block">
                <div class="wrap-title-black">
                    <h2 class="nice-title long">Разрядность Шины</h2>
                </div>
                <div class="block-content filter-block">
                    <form id="brand-form" action="" method="post">

                        <?php

                        foreach ($bandwidthList as $bandwidth) : ?>
                            <div class="checkline">
                                <input data-sql="band_<?= $bandwidth ?>" class="filterBox"
                                       type="checkbox" name="bandwidth"
                                       value="<?= $bandwidth ?>"/><span><?= $bandwidth ?></span>
                            </div>
                        <?php endforeach;
                        ?>

                    </form>
                </div>
            </div>

        </div>
        <div id="right-column">
            <div id="content">
                <div id="wrap-featured-products">
                    <div class="wrap-title-black">
                        <h1 class="nice-title">Видеокарты</h1>
                    </div>
                    <ul id="inline-product-list">

                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>