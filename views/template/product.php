<?php
return "
<li>
    <div class='product-photo'><a href='#'>
            <img src='images/cards/%s/%s.jpg'/></a></div>
    <div class='product-info'>
        <h3><a href='#'>%s</a></h3>
    </div>
    <div class='product-price'>
        <p>₴%s</p>
    </div>
    <p>Объем: <b>%s</b></p>
    <p>Разрядность: <b>%s</b></p>
</li>
";